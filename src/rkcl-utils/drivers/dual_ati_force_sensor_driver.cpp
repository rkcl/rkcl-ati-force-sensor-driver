/**
 * @file dual_ati_force_sensor_driver.cpp
 * @author Sonny Tarbouriech (LIRMM)
 * @brief Defines a utility class for rkcl to get data from two ati force sensors
 * @date 12-03-2020
 * License: CeCILL
 */
#include <rkcl/drivers/dual_ati_force_sensor_driver.h>

#include <rpc/devices/ati_force_sensor_udp_driver.h>

#include <yaml-cpp/yaml.h>
#include <iostream>

namespace rkcl
{

DualATIForceSensorDriver::DualATIForceSensorDriver(
    ObservationPointPtr op_left_eef,
    ObservationPointPtr op_right_eef,
    double cutoff_freq,
    std::string server_ip)
    : DualArmForceSensorDriver(op_left_eef, op_right_eef),
      server_ip_(std::move(server_ip)),
      cutoff_freq_(cutoff_freq)
{
}

DualATIForceSensorDriver::DualATIForceSensorDriver(Robot& robot, const YAML::Node& configuration)
    : DualArmForceSensorDriver(robot, configuration),
      server_ip_("192.168.0.20"),
      cutoff_freq_(-1)
{
    if (configuration)
    {
        const auto& server_ip = configuration["server_ip"];
        if (server_ip)
            server_ip_ = server_ip.as<std::string>();
        const auto& cutoff_freq = configuration["cutoff_frequency"];
        if (cutoff_freq)
            cutoff_freq_ = cutoff_freq.as<double>();
    }
}

DualATIForceSensorDriver::~DualATIForceSensorDriver() = default;

bool DualATIForceSensorDriver::init(double timeout)
{
    ati_driver_udp_client_ = std::make_unique<rpc::dev::ATIForceSensorAsyncUDPDriver>(
        server_ip_,
        phyq::Frequency{1000.},
        std::vector<phyq::Frame>{phyq::Frame{"FT1"}, phyq::Frame{"FT2"}},
        phyq::CutoffFrequency{cutoff_freq_});

    if (not ati_driver_udp_client_->connect())
    {
        std::cerr << "Failed to initialize the FT driver" << std::endl;
        return false;
    }
    return true;
}

bool DualATIForceSensorDriver::read()
{
    // Update wrench measured data and express them in the left and right end-effectors frame
    bool ok = ati_driver_udp_client_->read();
    leftEndEffectorPointState().wrench().block<3, 1>(0, 0) = op_left_eef_->state().pose().rotation() * ati_driver_udp_client_->device()[0].force().linear().value();
    leftEndEffectorPointState().wrench().block<3, 1>(3, 0) = op_left_eef_->state().pose().rotation() * ati_driver_udp_client_->device()[0].force().angular().value();
    rightEndEffectorPointState().wrench().block<3, 1>(0, 0) = op_right_eef_->state().pose().rotation() * ati_driver_udp_client_->device()[1].force().linear().value();
    rightEndEffectorPointState().wrench().block<3, 1>(3, 0) = op_right_eef_->state().pose().rotation() * ati_driver_udp_client_->device()[1].force().angular().value();

    return ok;
}

bool DualATIForceSensorDriver::stop()
{
    if (not ati_driver_udp_client_->disconnect())
    {
        std::cerr << "Failed to close the FT driver" << std::endl;
        return false;
    }
    return true;
}

} // namespace rkcl
