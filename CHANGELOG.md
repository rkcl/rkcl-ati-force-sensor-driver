# [](https://gite.lirmm.fr/rkcl/rkcl-ati-force-sensor-driver/compare/v2.1.0...v) (2022-06-28)



# [2.1.0](https://gite.lirmm.fr/rkcl/rkcl-ati-force-sensor-driver/compare/v2.0.1...v2.1.0) (2022-05-13)



## [2.0.1](https://gite.lirmm.fr/rkcl/rkcl-ati-force-sensor-driver/compare/v2.0.0...v2.0.1) (2021-10-06)


### Features

* updated dep to ati-force-sensor-driver ([2eff85d](https://gite.lirmm.fr/rkcl/rkcl-ati-force-sensor-driver/commits/2eff85d97fc0e39d0ef37afff343a1c7bc2b208a))



# [2.0.0](https://gite.lirmm.fr/rkcl/rkcl-ati-force-sensor-driver/compare/v1.1.0...v2.0.0) (2021-09-30)


### Features

* update ati force sensor driver dep ([3de726e](https://gite.lirmm.fr/rkcl/rkcl-ati-force-sensor-driver/commits/3de726eb7e4abd9b629f9b5a818e9b08d46e8f69))
* use conventional commits ([589ea9f](https://gite.lirmm.fr/rkcl/rkcl-ati-force-sensor-driver/commits/589ea9f8d3c47f9ae35741e7dbecaa02fb58b4db))



# [1.1.0](https://gite.lirmm.fr/rkcl/rkcl-ati-force-sensor-driver/compare/v1.0.1...v1.1.0) (2020-10-14)



## [1.0.1](https://gite.lirmm.fr/rkcl/rkcl-ati-force-sensor-driver/compare/v1.0.0...v1.0.1) (2020-03-12)



# 1.0.0 (2020-02-20)



