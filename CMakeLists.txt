CMAKE_MINIMUM_REQUIRED(VERSION 3.0.2)
set(WORKSPACE_DIR ${CMAKE_SOURCE_DIR}/../.. CACHE PATH "root of the packages workspace directory")
list(APPEND CMAKE_MODULE_PATH ${WORKSPACE_DIR}/share/cmake/system) # using generic scripts/modules of the workspace
include(Package_Definition NO_POLICY_SCOPE)

PROJECT(rkcl-ati-force-sensor-driver)

declare_PID_Package(
			AUTHOR      	Sonny Tarbouriech
			INSTITUTION		LIRMM
			ADDRESS 		git@gite.lirmm.fr:rkcl/rkcl-ati-force-sensor-driver.git
			PUBLIC_ADDRESS  https://gite.lirmm.fr/rkcl/rkcl-ati-force-sensor-driver.git
			YEAR        	2019
			LICENSE     	CeCILL
			DESCRIPTION 	ATI F/T sensor driver for RKCL
			CODE_STYLE		rkcl
			VERSION     	2.1.1
		)

PID_Author(AUTHOR Benjamin Navarro INSTITUTION LIRMM)

check_PID_Environment(TOOL conventional_commits)

PID_Category(driver)
PID_Publishing(PROJECT           https://gite.lirmm.fr/rkcl-ati-force-sensor-driver
               FRAMEWORK         rkcl-framework
               DESCRIPTION       ATI F/T sensor driver for RKCL
               ALLOWED_PLATFORMS x86_64_linux_abi11)

PID_Dependency(rkcl-core VERSION 2.1.0)
PID_Dependency(ati-force-sensor-driver VERSION 3.0.0)

build_PID_Package()
